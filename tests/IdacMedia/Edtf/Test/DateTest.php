<?php

namespace IdacMedia\Edtf\Test;

use IdacMedia\Edtf;

/**
 * Tests for fuzzy date functions/classes
 *
 * @author robert
 */
class DateTest extends \PHPUnit_Framework_TestCase
{

    public function testParsingValidDates()
    {
        $date = new Edtf\DateContainer('2012');
        $this->assertEquals(2012, $date->ranges[0]->year);

        $date = new Edtf\DateContainer('2012?');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(array('uncertain'), $date->ranges[0]->yearTags);

        $date = new Edtf\DateContainer('2012-05');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(5, $date->ranges[0]->month);

        $date = new Edtf\DateContainer('2012-21');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(21, $date->ranges[0]->month);

        $date = new Edtf\DateContainer('2012-05-21');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(5, $date->ranges[0]->month);
        $this->assertEquals(21, $date->ranges[0]->day);
    }

    public function testParsingValidDatePeriods()
    {
        $date = new Edtf\DateContainer('2012/2014');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(2014, $date->ranges[1]->year);

        $date = new Edtf\DateContainer('2012-05/2014-10');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(5, $date->ranges[0]->month);
        $this->assertEquals(2014, $date->ranges[1]->year);
        $this->assertEquals(10, $date->ranges[1]->month);

        $date = new Edtf\DateContainer('2012-21/2014-23');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(21, $date->ranges[0]->month);

        $date = new Edtf\DateContainer('2012-05-21/2014-10-21');
        $this->assertEquals(2012, $date->ranges[0]->year);
        $this->assertEquals(5, $date->ranges[0]->month);
        $this->assertEquals(21, $date->ranges[0]->day);
        
        $this->assertEquals(2014, $date->ranges[1]->year);
        $this->assertEquals(10, $date->ranges[1]->month);
        $this->assertEquals(21, $date->ranges[1]->day);
    }

    public function testParsingValidDateTimes()
    {
        $date = new Edtf\DateContainer('2001-02-03T09:30:01');
        $this->assertEquals(2001, $date->ranges[0]->year);
        $this->assertEquals(2, $date->ranges[0]->month);
        $this->assertEquals(3, $date->ranges[0]->day);
        $this->assertEquals(9, $date->ranges[0]->hours);
        $this->assertEquals(30, $date->ranges[0]->minutes);
        $this->assertEquals(1, $date->ranges[0]->seconds);
        $this->assertEquals(null, $date->ranges[0]->timezone);

        $date = new Edtf\DateContainer('2004-01-01T10:10:10Z');
        $this->assertEquals(2004, $date->ranges[0]->year);
        $this->assertEquals(1, $date->ranges[0]->month);
        $this->assertEquals(1, $date->ranges[0]->day);
        $this->assertEquals(10, $date->ranges[0]->hours);
        $this->assertEquals(10, $date->ranges[0]->minutes);
        $this->assertEquals(10, $date->ranges[0]->seconds);

        $date = new Edtf\DateContainer('2004-01-01T10:10:10+05:00');
        $this->assertEquals(2004, $date->ranges[0]->year);
        $this->assertEquals(1, $date->ranges[0]->month);
        $this->assertEquals(1, $date->ranges[0]->day);
        $this->assertEquals(10, $date->ranges[0]->hours);
        $this->assertEquals(10, $date->ranges[0]->minutes);
        $this->assertEquals(10, $date->ranges[0]->seconds);
        $this->assertEquals("+05:00", $date->ranges[0]->timezone);
    }

    public function testParsingValidDateTimeRanges()
    {
        $date = new Edtf\DateContainer('2001-02-03T09:30:01/2002-03-04T18:35:05');
        $this->assertEquals(2001, $date->ranges[0]->year);
        $this->assertEquals(2, $date->ranges[0]->month);
        $this->assertEquals(3, $date->ranges[0]->day);
        $this->assertEquals(9, $date->ranges[0]->hours);
        $this->assertEquals(30, $date->ranges[0]->minutes);
        $this->assertEquals(1, $date->ranges[0]->seconds);
        $this->assertEquals(null, $date->ranges[0]->timezone);

        $this->assertEquals(2002, $date->ranges[1]->year);
        $this->assertEquals(3, $date->ranges[1]->month);
        $this->assertEquals(4, $date->ranges[1]->day);
        $this->assertEquals(18, $date->ranges[1]->hours);
        $this->assertEquals(35, $date->ranges[1]->minutes);
        $this->assertEquals(5, $date->ranges[1]->seconds);
        $this->assertEquals(null, $date->ranges[1]->timezone);

        $date = new Edtf\DateContainer('2004-01-01T10:10:10Z/2005-03-04T18:35:05+02:00');
        $this->assertEquals(2004, $date->ranges[0]->year);
        $this->assertEquals(1, $date->ranges[0]->month);
        $this->assertEquals(1, $date->ranges[0]->day);
        $this->assertEquals(10, $date->ranges[0]->hours);
        $this->assertEquals(10, $date->ranges[0]->minutes);
        $this->assertEquals(10, $date->ranges[0]->seconds);
        $this->assertEquals("+00:00", $date->ranges[0]->timezone);

        $this->assertEquals(2005, $date->ranges[1]->year);
        $this->assertEquals(3, $date->ranges[1]->month);
        $this->assertEquals(4, $date->ranges[1]->day);
        $this->assertEquals(18, $date->ranges[1]->hours);
        $this->assertEquals(35, $date->ranges[1]->minutes);
        $this->assertEquals(5, $date->ranges[1]->seconds);
        $this->assertEquals("+02:00", $date->ranges[1]->timezone);

        $date = new Edtf\DateContainer('2004-01-01T10:10:10+05:00/2005-24');
        $this->assertEquals(2004, $date->ranges[0]->year);
        $this->assertEquals(1, $date->ranges[0]->month);
        $this->assertEquals(1, $date->ranges[0]->day);
        $this->assertEquals(10, $date->ranges[0]->hours);
        $this->assertEquals(10, $date->ranges[0]->minutes);
        $this->assertEquals(10, $date->ranges[0]->seconds);
        $this->assertEquals("+05:00", $date->ranges[0]->timezone);

        $this->assertEquals(2005, $date->ranges[1]->year);
        $this->assertEquals(24, $date->ranges[1]->month);
        $this->assertEquals(null, $date->ranges[1]->day);
        $this->assertEquals(null, $date->ranges[1]->hours);
        $this->assertEquals(null, $date->ranges[1]->minutes);
        $this->assertEquals(null, $date->ranges[1]->seconds);
        $this->assertEquals(null, $date->ranges[1]->timezone);
    }

    public function testDatePrecision()
    {
        $date = new Edtf\DateContainer('2012');
        $this->assertEquals('year', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012?');
        $this->assertEquals('year', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012-05');
        $this->assertEquals('month', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012-21');
        $this->assertEquals('season', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012-05-21');
        $this->assertEquals('day', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012-05-21T23:59:00');
        $this->assertEquals('time', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012-05-21T12:34:56Z');
        $this->assertEquals('timezone', $date->ranges[0]->getPrecision());

        $date = new Edtf\DateContainer('2012-05-21T00:00:00-08:30');
        $this->assertEquals('timezone', $date->ranges[0]->getPrecision());
    }

    public function testZerosInHoursIsNotTheSameAsNull()
    {
        $date = new Edtf\DateContainer('2012-05-21T00:00:00');
        $this->assertEquals('time', $date->ranges[0]->getPrecision());
    }
}
