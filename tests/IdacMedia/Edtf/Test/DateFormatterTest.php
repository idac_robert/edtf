<?php

namespace IdacMedia\Edtf\Test;

use IdacMedia\Edtf;

/**
 * Tests for formatting fuzzy date functions/classes
 *
 * @author robert
 */
class DateFormatterTest extends \PHPUnit_Framework_TestCase
{
    public function testFormattingDates()
    {
        $formatter = new Edtf\DateFormatter();
        $date = new Edtf\DateContainer('2012');

        $this->assertEquals('2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05');
        $this->assertEquals('May 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-21');
        $this->assertEquals('Spring 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05-21');
        $this->assertEquals('21 May 2012', $formatter->format($date));
    }

    public function testFormattingTemplatedDates()
    {
        $formatter = new Edtf\DateFormatter();
        $formatter->setTemplates(array(
            'year' => '{{year|Y}}',
            'month' => '{{month|M }}{{year|Y}}',
            'day' => '{{day|j/}}balzak{{month|n/}}cheroot{{year|Y}}',
        ));
        $formatter->setTrimChars(' /');

        $date = new Edtf\DateContainer('2012');
        $this->assertEquals('2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05');
        $this->assertEquals('May 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-21');
        $this->assertEquals('Spring 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05-21');
        $this->assertEquals('21/balzak5/cheroot2012', $formatter->format($date));
    }

    public function testFormattingDateInterval()
    {
        $formatter = new Edtf\DateFormatter();
        $formatter->setIntervalDelim(' to ');

        $date = new Edtf\DateContainer('2012/2014');
        $this->assertEquals('2012 to 2014', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-21/2014-23');
        $this->assertEquals('Spring 2012 to Autumn 2014', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05/2014-10');
        $this->assertEquals('May 2012 to October 2014', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05-21/2014-10-21');
        $this->assertEquals('21 May 2012 to 21 October 2014', $formatter->format($date));
    }

    public function testFormattingDateIntervalWithSharedUnits()
    {
        $formatter = new Edtf\DateFormatter();
        $formatter->setIntervalDelim(' to ');

        $date = new Edtf\DateContainer('2012/2012');
        $this->assertEquals('2012 to 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-21/2012-23');
        $this->assertEquals('Spring to Autumn 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05/2012-10');
        $this->assertEquals('May to October 2012', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-05-21/2012-05-22');
        $this->assertEquals('21 to 22 May 2012', $formatter->format($date));
    }

    public function testFormattingDateTimes()
    {
        $formatter = new Edtf\DateFormatter();
        $formatter->setIntervalDelim(' to ');

        $date = new Edtf\DateContainer('2012-11-10T00:00:00');
        $this->assertEquals('10 November 2012 00:00:00', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-11-10T00:00:00Z');
        $this->assertEquals('10 November 2012 00:00:00 UTC', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-11-10T00:00:00+01:00');
        $this->assertEquals('10 November 2012 00:00:00 +01:00', $formatter->format($date));
    }

    public function testFormattingDateTimeIntervals()
    {
        $formatter = new Edtf\DateFormatter();
        $formatter->setIntervalDelim(' to ');

        $date = new Edtf\DateContainer('2012-11-01/2012-11-10T00:00:00+01:00');
        $this->assertEquals('1 November 2012 to 10 November 2012 00:00:00 +01:00', $formatter->format($date));

        $date = new Edtf\DateContainer('2012-11-01T12:00:00/2012-11-10T00:00:00+01:00');
        $this->assertEquals('1 November 2012 12:00:00 to 10 November 2012 00:00:00 +01:00', $formatter->format($date));

        $formatter->setTemplates(array(
            'time' => '{{time|g.i a, }}{{day|j }}{{month|F }}{{year|Y}}',
        ));
        $formatter->setTrimChars(', ');
        $date = new Edtf\DateContainer('2012-11-01T12:00:00/2012-11-01T16:00:00');
        $this->assertEquals('12.00 pm to 4.00 pm, 1 November 2012', $formatter->format($date));
    }
}
