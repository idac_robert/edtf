<?php

namespace IdacMedia\Edtf;

/**
 * Represents a token for a season
 *
 * @author robert
 */
class SeasonToken extends UnitToken
{
    protected $seasons = array();

    public function __construct($value, FuzzyDate $date, array $seasons)
    {
        $this->seasons = $seasons;
        parent::__construct($value, $date);
    }

    public function __toString()
    {
        return str_replace('s', $this->seasons[$this->date->month], $this->value);
    }
}
