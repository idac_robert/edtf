<?php

namespace IdacMedia\Edtf;

/**
 * Represents a plain string token
 *
 * @author robert
 */
class TextToken extends Token
{
}
