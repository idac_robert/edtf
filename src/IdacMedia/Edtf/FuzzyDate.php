<?php

namespace IdacMedia\Edtf;

/**
 * Represents the range over which a specific date may be interpreted, based on an Extended Date/Time Format date.
 *
 * For example, a date '2012' may apply over 2012-01-01 to 2012-12-31.
 *
 * @author robert@idacmedia.com
 */
class FuzzyDate
{
    /**
     * @var \DateTime
     */
    public $start;
    
    /**
     * @var \DateTime
     */
    public $end;
    
    /**
     * @var int
     */
    public $year = null;
    
    /**
     * @var int
     */
    public $month = null;
    
    /**
     * @var int
     */
    public $day = null;
    
    /**
     * @var int
     */
    public $hours = null;

    /**
     * @var int
     */
    public $minutes = null;

    /**
     * @var int
     */
    public $seconds = null;

    /**
     * @var string
     */
    public $timezone = null;

    /**
     * @var array
     */
    public $yearTags = array();
    
    /**
     * @var array
     */
    public $monthTags = array();
    
    /**
     * @var array
     */
    public $dayTags = array();
    
    /**
     * Stores start days for seasons.
     * 
     * Assumes northern hemisphere, apparently spec is incomplete here.
     * 
     * @var type 
     */
    protected $seasons = array(
        21 => array( // Spring
            'month' => 3,
            'day' => 21,
        ),
        22 => array( // Summer
            'month' => 6,
            'day' => 21,
        ),
        23 => array( // Autumn
            'month' => 9,
            'day' => 22,
        ),
        24 => array( // Winter
            'month' => 12,
            'day' => 21,
        ),
    );

    /**
     *
     * @param string $dateString
     */
    public function __construct($dateString)
    {
        $toks = explode("-", $dateString);

        // First check if cleaned date matches a full ISO date/time string:
        $cleanDateString = preg_replace("/[^0-9TZ+-:]/", "", $dateString);
        if (preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|[+-]\d{2}:\d{2})?$/', $cleanDateString) > 0) {
            // we have an ISO 8601 date time:
            $this->start = new \DateTime($cleanDateString);
            $this->end = new \DateTime($cleanDateString);

            $this->year = (int) $this->start->format('Y');
            $this->month = (int) $this->start->format('n');
            $this->day = (int) $this->start->format('j');
            $this->hours = (int) $this->start->format('G');
            $this->minutes = (int) $this->start->format('i');
            $this->seconds = (int) $this->start->format('s');
            $this->timezone = $this->start->format('P');

            // Detect whether timezone has actually been specified:
            if (preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(Z|[+-]\d{2}:\d{2})$/', $cleanDateString) === 0) {
                $this->timezone = null;
            } elseif (strpos($cleanDateString, 'Z') > 0) {
                // Make UTC timezone format more meaningfully:
                $this->start->setTimezone(new \DateTimeZone('UTC'));
                $this->end->setTimezone(new \DateTimeZone('UTC'));
            }

            // TODO parse out uncertainty tokens
        } elseif (count($toks) > 0) {
            $year_string = $toks[0];
            if ($year_string == "" && count($toks) > 1) {
                $year_string = "-" . $toks[1];
                array_shift($toks);
            }
            $month_string = null;
            $day_string = null;
            if (count($toks) > 1) {
                $month_string = $toks[1];
            }
            if (count($toks) > 2) {
                $day_string = $toks[2];
            }
            if (strpos($year_string, "?") != false) {
                $this->yearTags[] = "uncertain";
            }
            if ($day_string != null && $month_string != null) {
                if (strpos($month_string, "?") != false) {
                    $this->monthTags[] = "uncertain";
                }
                if (strpos($day_string, "?") != false) {
                    $this->dayTags[] = "uncertain";
                }
                
                $this->year = (int) $year_string;
                $this->month = (int) $month_string;
                $this->day = (int) $day_string;

                $this->start = new \DateTime();
                $this->start->setDate((int) $year_string, (int) $month_string, (int) $day_string);
                $this->start->setTime(0, 0, 0);

                $this->end = new \DateTime();
                $this->end->setDate((int) $year_string, (int) $month_string, (int) $day_string);
                $this->end->setTime(23, 59, 59);
            } elseif ($month_string != null) {
                if (strpos($month_string, "?") != false) {
                    $this->monthTags[] = "uncertain";
                }

                $this->month = (int) $month_string;
                $this->year = (int) $year_string;
                if ($this->month >= 21 && $this->month <= 24) {
                    // Represents a season, not a month
                    $this->monthTags[] = 'season';
                    $this->start = new \DateTime();
                    $startDate = $this->seasons[$this->month];
                    $this->start->setDate($this->year, $startDate['month'], $startDate['day']);
                    $this->start->setTime(0, 0, 0);

                    $nextSeasonNum = (($this->month - 21 + 1) % 4) + 21;
                    $nextYear = $nextSeasonNum === 21 ? $this->year + 1 : $this->year;
                    $nextSeason = $this->seasons[$nextSeasonNum];

                    $this->end = new \DateTime();
                    $this->end->setDate($nextYear, $nextSeason['month'], $nextSeason['day']);
                    $this->end->setTime(0, 0, 0);
                    $this->end->sub(new \DateInterval("PT1S"));
                } else {
                    // Ordinary month number
                    $this->start = new \DateTime();
                    $this->start->setDate($this->year, $this->month, 1);
                    $this->start->setTime(0, 0, 0);

                    $next_month = $this->month;
                    $next_year = $this->year;

                    if ($next_month === 12) {
                        $next_month = 1;
                        $next_year = $next_year + 1;
                    } else {
                        $next_month = $next_month + 1;
                    }

                    $this->end = new \DateTime();
                    $this->end->setDate($next_year, $next_month, 1);
                    $this->end->setTime(0, 0, 0);

                    $this->end->sub(new \DateInterval("PT1S"));
                }
            } else {
                if (strlen($year_string) == 2) {
                    $year_string = $year_string . "00";
                    $this->year = (int) $year_string;

                    $this->start = new \DateTime();
                    $this->start->setDate($this->year, 1, 1);
                    $this->start->setTime(0, 0, 0);

                    $this->end = new \DateTime();
                    $this->end->setDate(intval($year_string) + 100, 1, 1);
                    $this->end->setTime(0, 0, 0);

                    $this->end->sub(new \DateInterval("PT1S"));
                } else {
                    $this->year = (int) $year_string;
                    $this->start = new \DateTime();
                    $this->start->setDate($this->year, 1, 1);
                    $this->start->setTime(0, 0, 0);

                    $this->end = new \DateTime();
                    $this->end->setDate($this->year + 1, 1, 1);
                    $this->end->setTime(0, 0, 0);

                    $this->end->sub(new \DateInterval("PT1S"));
                }
            }
        }
    }

    /**
     * Returns the precision of the date, for example, year, month, or down to
     * timezone.
     *
     * @return string A precision, one of year, season, month, day, time or timezone.
     */
    public function getPrecision()
    {
        if ($this->timezone) {
            $precision = 'timezone';
        } elseif ($this->hours !== null) {
            $precision = 'time';
        } elseif ($this->day) {
            $precision = 'day';
        } elseif ($this->month) {
            if ($this->month > 20) {
                $precision = 'season';
            } else {
                $precision = 'month';
            }
        } elseif ($this->year) {
            $precision = 'year';
        }

        return $precision;
    }
}
