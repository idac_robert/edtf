<?php

namespace IdacMedia\Edtf;

/**
 * Represents a token for a unit of time
 *
 * @author robert
 */
class UnitToken extends Token
{
    protected $date;

    public function __construct($value, FuzzyDate $date)
    {
        parent::__construct($value);
        $this->date = $date;
    }

    public function __toString()
    {
        return $this->date->start->format($this->value);
    }
}
