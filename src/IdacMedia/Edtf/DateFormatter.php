<?php

namespace IdacMedia\Edtf;

/**
 * Description of DateFormatter
 *
 * @author Rob-C
 */
class DateFormatter
{
    /**
     *
     * @var DateContainer
     */
    protected $date;
    
    protected $templates = array(
        'year' => '{{year|Y}}',
        'month' => '{{month|F }}{{year|Y}}',
        'day' => '{{day|j }}{{month|F }}{{year|Y}}',
        'time' => '{{day|j }}{{month|F }}{{year|Y }}{{time|H:i:s }}',
        'timezone' => '{{day|j }}{{month|F }}{{year|Y }}{{time|H:i:s }}{{timezone|e}}',
        'season' => '{{season|s }}{{year|Y}}',
    );

    protected $intervalDelim = '-';
    protected $listDelim = ', ';

    protected $trimChars = ' ';


    public static $seasons = array(
        21 => 'Spring',
        22 => 'Summer',
        23 => 'Autumn',
        24 => 'Winter',
    );

    const YEAR = 0x1;
    const SEASON = 0x2;
    const MONTH = 0x4;
    const DAY = 0x8;
    const TIME = 0x10;
    const TIMEZONE = 0x20;


    public function __construct($templates = array())
    {
        $this->setTemplates($templates);
    }

    /**
     *
     * @param array $templates Keyed by year, month and day
     */
    public function setTemplates($templates = array())
    {
        $templates = array_merge($this->templates, $templates);
        $this->templates = $templates;
    }

    /**
     * Sets characters to be trimmed when not all values are specified, such as spaces or slashes.
     *
     * @param string $trimChars
     */
    public function setTrimChars($trimChars)
    {
        $this->trimChars = $trimChars;
    }

    public function setIntervalDelim($rangeDelim)
    {
        $this->intervalDelim = $rangeDelim;
    }

    public function setListDelim($listDelim)
    {
        $this->listDelim = $listDelim;
    }

    /**
     * Return passed date as a human-readable string.
     *
     * @return string
     */
    public function format(DateContainer $date)
    {
        $buffer = "";
        $delim = $this->intervalDelim;

        $rangeFormats = array();
        foreach ($date->ranges as $range) {
            $rangeFormats[] = array(
                'template' => $range->getPrecision(),
                'flags' => -1,
            );
        }
        if (count($date->ranges) === 2) {
            // Only attempt consolidating time units if precision of both dates
            // is the same:
            if ($date->ranges[0]->getPrecision() === $date->ranges[1]->getPrecision()
                && $date->ranges[0]->month
                && $date->ranges[0]->year === $date->ranges[1]->year) {
                $rangeFormats[0]['flags'] &= ~DateFormatter::YEAR;
                if ($date->ranges[0]->day
                    && $date->ranges[0]->month === $date->ranges[1]->month) {
                    $rangeFormats[0]['flags'] &= ~DateFormatter::MONTH;
                    if ($date->ranges[0]->hours !== null
                        && $date->ranges[0]->day === $date->ranges[1]->day) {
                        $rangeFormats[0]['flags'] &= ~DateFormatter::DAY;
                    }
                }
            }
            $buffer .= $this->formatDateRange(
                $date->ranges[0],
                $this->templates[$rangeFormats[0]['template']],
                $rangeFormats[0]['flags']
            );
            $buffer .= $delim;
            $buffer .= $this->formatDateRange(
                $date->ranges[1],
                $this->templates[$rangeFormats[1]['template']],
                $rangeFormats[1]['flags']
            );
        } elseif (count($date->ranges) === 1) {
            $buffer = $this->formatDateRange(
                $date->ranges[0],
                $this->templates[$rangeFormats[0]['template']],
                $rangeFormats[0]['flags']
            );
        } else {
            throw new \Exception('Formatting multiple dates not implemented');
        }
        
        return $buffer;
    }
    
    /**
     * Formats a date according to the template, and visibility flags passed
     *
     * @param \IdacMedia\Edtf\FuzzyDate $date
     * @param string $template
     * @param int $unitFlags Set according to class constants
     * @return string
     */
    protected function formatDateRange(FuzzyDate $date, $template, $unitFlags = -1)
    {
        preg_match_all('/({{(\w+)(|[^}]+)?}})/', $template, $tokens, PREG_OFFSET_CAPTURE);

        $parsed = array();
        $pos = 0;
        foreach ($tokens[2] as $i => $token) {
            $tokenLength = \strlen($tokens[0][$i][0]);
            $tokenPos = $tokens[0][$i][1];
            if ($tokenPos - $pos > 0) {
                $parsed[] = new TextToken(substr($template, $pos, $tokenPos - $pos));
            }
            switch ($token[0]) {
                case 'year':
                    if ($unitFlags & DateFormatter::YEAR) {
                        $parsed[] = new UnitToken(ltrim($tokens[3][$i][0], '|'), $date);
                    }
                    break;
                case 'season':
                    if ($unitFlags & DateFormatter::SEASON) {
                        $parsed[] = new SeasonToken(ltrim($tokens[3][$i][0], '|'), $date, self::$seasons);
                    }
                    break;
                case 'month':
                    if ($unitFlags & DateFormatter::MONTH) {
                        $parsed[] = new UnitToken(ltrim($tokens[3][$i][0], '|'), $date);
                    }
                    break;
                case 'day':
                    if ($unitFlags & DateFormatter::DAY) {
                        $parsed[] = new UnitToken(ltrim($tokens[3][$i][0], '|'), $date);
                    }
                    break;
                case 'time':
                    if ($unitFlags & DateFormatter::TIME) {
                        $parsed[] = new UnitToken(ltrim($tokens[3][$i][0], '|'), $date);
                    }
                    break;
                case 'timezone':
                    if ($unitFlags & DateFormatter::TIMEZONE) {
                        $parsed[] = new UnitToken(ltrim($tokens[3][$i][0], '|'), $date);
                    }
                    break;
            }
            $pos = $tokenPos + $tokenLength;
        }
        if (\strlen($template) - $pos > 0) {
            $parsed[] = new TextToken(substr($template, $pos, \strlen($template) - $pos));
        }

        return trim(implode('', $parsed), $this->trimChars);
    }
}
