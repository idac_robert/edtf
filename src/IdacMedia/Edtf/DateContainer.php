<?php

namespace IdacMedia\Edtf;

/**
 * Description of Date
 *
 * @author Rob-C
 */
class DateContainer
{
    /**
     *
     * @var FuzzyDate[]
     */
    public $ranges = array();
    
    public function __construct($dateString)
    {
        $this->setDate($dateString);
    }

    /**
     * 
     * @todo Implements part of the level 0 and level 1 standards
     * @link http://www.loc.gov/standards/datetime/pre-submission.html
     * @param string $dateString Fuzzy date string
     * @return void
     */
    public function setDate($dateString)
    {
        $this->ranges = array();

        $bracket_start = strpos($dateString, "{");

        if ($bracket_start !== false) {
            $bracket_end = strrpos($dateString, "}");

            $toks = explode(",", substr($dateString, $bracket_start + 1, ($bracket_end - $bracket_start - 1)));

            foreach ($toks as $tok) {
                $sub_range = new DateContainer($tok);
                $this->ranges = array_merge($this->ranges, $sub_range->ranges);
            }
        } elseif (strpos($dateString, "/") !== false) {
            $toks = explode("/", $dateString);
            if (strlen($toks[0]) > 0) {
                $this->ranges[] = new FuzzyDate($toks[0]);
            }

            if (strlen($toks[1]) > 0) {
                $this->ranges[] = new FuzzyDate($toks[1]);
            }
        } else {
            $this->ranges[] = new FuzzyDate($dateString);
        }
    }
}
