<?php

namespace IdacMedia\Edtf;

/**
 * Represents a token in a date template string
 */
abstract class Token
{
    protected $value = '';

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return "" . $this->value;
    }
}
